public class Node {
    public Node leftChild, rightChild;
    public char data;
    
    public Node (char gamma, Node alpha, Node beta) {
        this.leftChild = alpha;
        this.rightChild = beta;
        this.data = gamma;
    }
    
    public Node (char gamma) {
        this.leftChild = null;
        this.rightChild = null;
        this.data = gamma;
    }
}
