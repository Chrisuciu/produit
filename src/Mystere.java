import java.util.Stack;

public class Mystere {
    private Node laChose;
    private StringBuilder incantation;
    
    public Mystere (Node uneChose) {
        this.laChose = uneChose;
    }
    
    public String magie () {
        incantation = new StringBuilder();
        final Node marqueurMachin = new Node(')');
        Stack<Node> chosesIncompletes = new Stack<>();
        
        Node choseCourante = laChose;
        do {
            if (estBizarre(choseCourante)) {
                augmenterMagie(choseCourante.data);
                
                do {
                    if (chosesIncompletes.isEmpty())
                        return incantation.toString();
                    choseCourante = chosesIncompletes.pop();
                    diminuerMagie(choseCourante.data);
                } while (choseCourante == marqueurMachin);
                
                choseCourante = choseCourante.rightChild;
            }
            else {
                incantation.append('(');
                chosesIncompletes.push(marqueurMachin);
                chosesIncompletes.push(choseCourante);
                choseCourante = choseCourante.leftChild;
            }
        } while (choseCourante != null);
        
        throw new RuntimeException("Le truc a imprimer est incomplet.");
    }
    
    private boolean estBizarre (Node c) {
        return c.leftChild == null && c.rightChild == null;
    }
    
    private void augmenterMagie (char c) {
        if (Character.isDigit(c))
            incantation.append(c);
        else
            throw new RuntimeException("La chose " + c + " n'est pas un chiffre.");
    }
    
    private void diminuerMagie (char c) {
        if (!Character.isDigit(c))
            incantation.append(c);
        else
            throw new RuntimeException("La chose " + c + " est un chiffre.");
    }
}
